from django.utils.timezone import now
from rest_framework import serializers

from location.models import Position, Path


class PathSerializer(serializers.ModelSerializer):
    class Meta:
        model = Path
        fields = ('name', 'time', 'distance', 'average_speed', 'id')


class PositionSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField(read_only=True)

    def get_age(self, obj):
        return (now() - obj.time).seconds

    class Meta:
        model = Position
        fields = ('lat', 'lng', 'speed', 'heading', 'path', 'age')