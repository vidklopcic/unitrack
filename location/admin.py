from django.contrib import admin

# Register your models here.
from location.models import Path, Position

my_models = [Path, Position]
admin.site.register(my_models)