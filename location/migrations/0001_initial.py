# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-26 17:41
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Path',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=250)),
                ('time', models.DateTimeField(auto_now=True)),
                ('distance', models.IntegerField(default=0)),
                ('average_speed', models.IntegerField(default=0)),
            ],
        ),
        migrations.CreateModel(
            name='Position',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lat', models.FloatField()),
                ('lng', models.FloatField()),
                ('speed', models.FloatField()),
                ('heading', models.FloatField()),
                ('time', models.DateTimeField(auto_now=True)),
                ('path', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='location.Path')),
            ],
        ),
    ]
