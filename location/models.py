from __future__ import unicode_literals

from django.db import models

# Create your models here.
from UniTrack import settings


class Path(models.Model):
    name = models.CharField(max_length=250)
    time = models.DateTimeField(auto_now=True)
    distance = models.IntegerField(default=0)
    average_speed = models.IntegerField(default=0)
    user = models.ForeignKey(settings.AUTH_USER_MODEL)

    def __str__(self):
        return self.name


class Position(models.Model):
    lat = models.FloatField()
    lng = models.FloatField()
    speed = models.FloatField()
    heading = models.FloatField()
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    path = models.ForeignKey(Path)
    time = models.DateTimeField(auto_now=True)
