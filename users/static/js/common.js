var tooltip_right = $('<div style="position: absolute; z-index: 999; display: none; height: 30px; color: white; border-radius: 5px; background-color: #4E4E4E"><div style="display: inline-block; position: absolute; left: -9px; top: 10px;" class="arrow-left"></div><span style="vertical-align: middle; display: table-cell; padding: 0 10px 0 10px; height: 30px;"></span></div>');
var tooltip_top = $('<div style="position: absolute; text-align: center; display: none; z-index: 999; height: 30px; color: white; border-radius: 5px; background-color: #4E4E4E"><span style="vertical-align: middle; display: table-cell; padding: 0 10px 0 10px; height: 30px;"></span><div style="font-size: 0;"><div style="display: inline-block;" class="arrow-down"></div></div></div>');
function common_init() {
    $("html").append(tooltip_right);
    $("html").append(tooltip_top);
    tooltips();
}

function hideTooltips() {
    tooltip_top.find('.arrow-down').css('transform', 'translateX(0)');
    tooltip_top.hide();
    tooltip_right.hide();
}

function showWindow(selectorOrEl) {
    var element;
    if (typeof(selectorOrEl) === 'string') {
        element = $(selectorOrEl);
    } else if (typeof(selectorOrEl) === 'object') {
        element = selectorOrEl;
    } else {
        return;
    }
    element.show();
    element.velocity(
        {translateY: "50px"},
        {duration: 0});
    element.velocity(
        {opacity: "1", translateY: "0"},
        {duration: 500, easing: [60, 10]});
}

function hideWindow(selectorOrEl) {
    var element;
    if (typeof(selectorOrEl) === 'string') {
        element = $(selectorOrEl);
    } else if (typeof(selectorOrEl) === 'object') {
        element = selectorOrEl;
    } else {
        return;
    }

    element.velocity(
        {opacity: "0", translateY: "-50"},
        {duration: 500, easing: [60, 10]});
    setTimeout(function () {
        element.hide();
    }, 500);
}

function tooltips() {
    $.each($('.tooltip-right'), function (index, element) {
        $(element).hover(function () {
            var $this = $(this);
            var content = $this.attr('tooltip');
            tooltip_right.find('span').html(content);
            var offset = $this.offset();
            offset.top += $this.outerHeight() / 2 - tooltip_right.outerHeight() / 2;
            offset.left += $this.outerWidth() + 10;
            tooltip_right.css(offset);
            tooltip_right.show();
        }, function () {
            tooltip_right.hide();
        });
    });

    $.each($('.tooltip-top'), function (index, element) {
        $(element).hover(function () {
            var $this = $(this);
            var content = $this.attr('tooltip');
            tooltip_top.find('span').html(content);
            var offset = $this.offset();
            offset.top -= $this.outerHeight();
            offset.left += $this.outerWidth() / 2 - tooltip_top.outerWidth() / 2;
            if (offset.left < 0) {
                tooltip_top.find('.arrow-down').css('transform', 'translateX(' + (offset.left - 10) + 'px)');
                offset.left = 10;
            }
            tooltip_top.css(offset);
            tooltip_top.show();
        }, function () {
            tooltip_top.find('.arrow-down').css('transform', 'translateX(0)');
            tooltip_top.hide();
        });
    });

    $.each($('.hide-clicked-tooltip'), function (index, element) {
        $(element).click(function () {
            hideTooltips();
        });
    })
}