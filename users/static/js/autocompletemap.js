var map;
var marker;
var icon;

function startMap() {
    if (map != null) {
        return;
    }
    map = new google.maps.Map(document.getElementById('map'), {
        disableDefaultUI: true,
        center: {lat: 46.0493133, lng: 14.51},
        zoom: 12
    });
    if (base_lat != null) {
        var bounds = new google.maps.LatLngBounds();
        bounds.extend(new google.maps.LatLng(base_lat, base_lng));
        map.fitBounds(bounds);
    }
    icon = {
        url: home_ico_url,
        anchor: new google.maps.Point(13, 13),
        scaledSize: new google.maps.Size(25, 25)
    };
    marker = new google.maps.Marker({
        map: map,
        icon: icon,
        draggable: true,
        position: {lat: base_lat, lng: base_lng}
    });
    initAutocomplete();
}

function initAutocomplete() {
    // Create the search box and link it to the UI element.
    var input = document.getElementById('base-address');
    var searchBox = new google.maps.places.SearchBox(input);
    input.placeholder = "";

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function () {
        searchBox.setBounds(map.getBounds());
    });

    // Listen for the event fired when the user selects a prediction and retrieve
    // more details for that place.
    searchBox.addListener('places_changed', function () {
        var places = searchBox.getPlaces();

        if (places.length == 0 || places.length > 1) {
            return;
        }

        // Clear out the old markers.
        if (marker != null) {
            marker.setMap(null);
        }

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();

        if (!places[0].geometry) {
            console.log("Returned place contains no geometry");
            return;
        }
        marker = new google.maps.Marker({
            map: map,
            icon: icon,
            draggable: true,
            title: places[0].name,
            position: places[0].geometry.location
        });

        if (places[0].geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(places[0].geometry.viewport);
        } else {
            bounds.extend(places[0].geometry.location);
        }
        map.fitBounds(bounds);
    });
}

function saveBaseLocation() {
    $.post(api_set_base, {
        "lat": marker.getPosition().lat(),
        "lng": marker.getPosition().lng()
    }, function (data) {
        if (data == 'ok') {
            Materialize.toast('base location updated', 3000);
        } else {
            Materialize.toast('base location update failed', 3000);
        }
    });
}