from django.contrib.auth import authenticate
from django.contrib.auth.decorators import permission_required
from django.contrib.auth.models import Group
from django.shortcuts import render, HttpResponse
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.decorators import api_view, authentication_classes, permission_classes
from rest_framework.permissions import IsAuthenticated

from login.models import TrackUser, TrackUserManager


@permission_required('login.can_add_driver')
def users(request):
    is_superuser = False
    if request.user.company.name == "superuser":
        users = TrackUser.objects.filter(groups__name="companies")
        is_superuser = True
    else:
        users = TrackUser.objects.filter(groups__name="drivers", company=request.user.company)
    return render(request, 'users.html', {'users': users, 'superuser': is_superuser})


@permission_required('login.can_add_driver')
def list_users(request):
    is_superuser = False
    if request.user.company.name == "superuser":
        users = TrackUser.objects.filter(groups__name="companies")
        is_superuser = True
    else:
        users = TrackUser.objects.filter(groups__name="drivers", company=request.user.company)
    return render(request, 'user_list.html', {'users': users, 'superuser': is_superuser})


@permission_required('login.can_add_driver')
def delete_user(request, email=""):
    user = TrackUser.objects.filter(email=email, company=request.user.company)
    if len(user) > 0 and not user.first().is_superuser:
        user.first().delete()
        return HttpResponse("deleted")
    return HttpResponse("error")


@permission_required('login.can_add_driver')
def create_driver(request):
    driver = TrackUser.objects.filter(email=request.POST['old-mail'], company=request.user.company)
    tm = TrackUserManager()
    if len(driver) == 1:
        driver = driver[0]
        driver.email = request.POST['email']
        driver.first_name = request.POST['first-name']
        driver.last_name = request.POST['last-name']
        driver.nickname = request.POST['nickname']
        if authenticate(email=request.POST['email'], password=request.POST['password']):
            driver.set_password(request.POST['new-password'])
        if len(request.FILES) > 0:
            driver.profile_picture = request.FILES['profile']
            driver.resize_profile_picture()
        driver.save()
    else:
        driver = tm.create_user(request.POST['email'], request.POST['password'], request.user.company)
        driver.first_name = request.POST['first-name']
        driver.last_name = request.POST['last-name']
        driver.nickname = request.POST['nickname']
        if len(request.FILES) > 0:
            driver.profile_picture = request.FILES['profile']
            driver.resize_profile_picture()
        if request.user.is_superuser:
            g = Group.objects.get(name='companies')
        else:
            g = Group.objects.get(name='drivers')
        g.user_set.add(driver)
        g.save()
        driver.save()
    return HttpResponse("ok")


@permission_required('login.can_add_driver')
def settings(request):
    return render(request, 'admin-settings.html', context={'token': Token.objects.get(user=request.user),
                                                           'user': request.user})


@api_view(['POST'])
def set_base_location(request):
    request.user.base_lat = request.POST['lat']
    request.user.base_lng = request.POST['lng']
    request.user.save()
    return HttpResponse('ok')


@permission_required('login.can_add_driver')
def update_admin(request):
    auth_failed = False
    user = request.user
    user.email = request.POST['email']
    user.first_name = request.POST['first-name']
    user.last_name = request.POST['last-name']
    if request.POST['password'] and request.POST['new-password']:
        if authenticate(email=request.POST['email'], password=request.POST['password']):
            user.set_password(request.POST['new-password'])
        else:
            auth_failed = True
    if len(request.FILES) > 0:
        user.profile_picture = request.FILES['profile']
        user.resize_profile_picture()
    user.save()
    if auth_failed:
        return HttpResponse('auth failed')
    else:
        return HttpResponse('ok')
