from django.conf.urls import url
import users.views

urlpatterns = [
    url(r'^$', users.views.users, name='users'),
    url(r'^settings/$', users.views.settings, name='admin-settings'),
    url(r'^list/$', users.views.list_users, name='list-users'),
    url(r'^delete/(?P<email>.+)', users.views.delete_user, name='delete-user'),
    url(r'^delete/$', users.views.delete_user, name='delete-user'),
    url(r'^create/$', users.views.create_driver, name='create-update-driver'),
    url(r'^set-base-location/$', users.views.set_base_location, name='set-base-location'),
    url(r'^update-admin-data/$', users.views.update_admin, name='update-admin-data'),
]
