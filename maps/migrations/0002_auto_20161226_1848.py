# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-26 17:48
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('maps', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mapstate',
            name='lat1',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='mapstate',
            name='lat2',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='mapstate',
            name='lng1',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='mapstate',
            name='lng2',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='viewlocation',
            name='lat1',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='viewlocation',
            name='lat2',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='viewlocation',
            name='lng1',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='viewlocation',
            name='lng2',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
