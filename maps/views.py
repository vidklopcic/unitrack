from django.contrib.auth import logout
from django.contrib.auth.decorators import login_required
from django.contrib.staticfiles.templatetags.staticfiles import static
from django.shortcuts import render, HttpResponse

# Create your views here.
from rest_framework.authtoken.models import Token

from maps.models import ViewLocation


@login_required
def index(request):
    context = {'token': Token.objects.get(user=request.user),
               'positions': ViewLocation.objects.filter(user=request.user)}
    return render(request, 'maps.html', context)


@login_required
def get_positions(request):
    return render(request, 'center_positions.html', {'positions': ViewLocation.objects.filter(user=request.user)})
