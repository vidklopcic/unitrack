from rest_framework import serializers

from maps.models import MapState, ViewLocation


class MapStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = MapState
        fields = ('view_mode', 'lat1', 'lng1', 'lat2', 'lng2')


class ViewLocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = ViewLocation
        fields = ('name', 'lat1', 'lng1', 'lat2', 'lng2')
