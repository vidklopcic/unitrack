var icon_path = 'M10 0c-5.531 0-10 4.469-10 10s5 13 10 22c5-9 10-16.469 10-22s-4.469-10-10-10zM10 14c-2.219 0-4-1.781-4-4s1.781-4 4-4 4 1.781 4 4-1.781 4-4 4z';
var location_icon = {
    path: icon_path,
    fillColor: '#4497FF',
    strokeColor: '#3074E8',
    fillOpacity: 1.3,
    scale: 1,
    rotation: 0
};

var info_box_options;
var info_box_label_offset;
var info_box_img_offset;
var locations;
var markers = {};
var max_age = 20;
var trail_length = 30;
var trail;
var counter = 0;
var infobox_label = '<div class="flex-column" style="justify-content: center"><div class="flex-row" style="width:100px;justify-content: center;-webkit-text-stroke: 1px white; color: blue; font-size: 15px; font-weight: bold;">{name}</div></div>';
var infobox_img = '<div class="circle" style="width: 30px; height:30px;background-image: url({url}); background-size: cover; vertical-align: middle; flex-grow: 1;"></div>'

function startMarkers() {
    location_icon['anchor'] = new google.maps.Point(10, 10);
    info_box_label_offset = new google.maps.Size(-50, 5);
    info_box_img_offset = new google.maps.Size(-15, -15);
    info_box_options = {
        disableAutoPan: true,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(0, 0),
        zIndex: null,
        boxStyle: {
            padding: "0px 0px 0px 0px",
            width: "252px",
            height: "40px"
        },
        closeBoxURL: "",
        infoBoxClearance: new google.maps.Size(1, 1),
        isHidden: false,
        pane: "floatPane",
        enableEventPropagation: true
    };
    if (base_lng) {
        var icon = {
            url: home_ico_url,
            anchor: new google.maps.Point(20, 20),
            scaledSize: new google.maps.Size(40, 40)
        };
        new google.maps.Marker({
            map: map,
            icon: icon,
            position: {lat: base_lat, lng: base_lng}
        });
    }
    updateOrInit();
}

function updateOrInit() {
    counter += 1;
    $.getJSON(api_get_location, function (data) {
        locations = data;
        var emails = [];
        $.each(data, function (i, val) {
            if (val['location']['lat'] != null && val['location']['age'] < max_age) {
                val['location']['heading'] -= 180;
                var email = val['user']['email'];
                emails.push(email);
                if (email in markers) {
                    if ('marker' in markers[email])
                        var marker = markers[email]['marker'];
                    markers[email].user = val.user;
                    markers[email].location = val.location;
                } else {
                    markers[email] = val;
                    markers[email]['trail'] = new google.maps.Polyline({
                        strokeColor: '#4497FF',
                        strokeOpacity: 1.0,
                        strokeWeight: 3
                    });
                    markers[email].trail.setMap(map);
                    markers[email].trail.getPath().length = trail_length;
                }
                markers[email]['marker'] = marker;
            }
        });
        $.each(markers, function (key, val) {
            if ($.inArray(key, emails) == -1) {
                markers[key]['marker'].setMap(null);
                markers[key].infobox_img.close();
                markers[key].infobox_label.close();
                markers[key].trail.setMap(null);
                delete markers[key];
            }
        });
        updateMarkers();
    });

    setTimeout(updateOrInit, 1000);
}

function updateMarkers() {
    var bounds = new google.maps.LatLngBounds();
    if (base_lng) {
        bounds.extend(new google.maps.LatLng(base_lat, base_lng));
    }
    $.each(markers, function (key, val) {
        bounds.extend(val.location);
        if (val['marker']) {
            val['marker'].setPosition(val['location']);
            location_icon.rotation = val['location']['heading'];
            val['marker'].setIcon(location_icon);
            var trail = val.trail.getPath();
            trail.push(new google.maps.LatLng(val.location.lat, val.location.lng));
            if (trail.length > trail_length)
                trail.removeAt(0);
        } else {
            val['marker'] = new google.maps.Marker({
                position: val.location,
                map: map
            });
            displayInfobox(val);
            location_icon.rotation = val['location']['heading'];
            val['marker'].setIcon(location_icon);
        }
    });
    if (view_mode == AUTO_FIT) {
        map.fitBounds(bounds);
    }
}

function hideInfobox() {
    $.each(markers, function (key, val) {
        try {
            val.infobox_img.close();
            val.infobox_label.close();
        } catch (e) {}
    });
}

function redrawInfobox() {
    $.each(markers, function (key, val) {
        try {
            val.infobox_img.close();
            val.infobox_label.close();
            displayInfobox(val);
        } catch (e) {}
    });
}

function displayInfobox(val) {
    info_box_options.content = infobox_img.replace('{url}', val.user.profile_picture);
    info_box_options.pixelOffset = info_box_img_offset;
    ib = new InfoBox(info_box_options);
    ib.open(map, val.marker);
    val.infobox_img = ib;

    if (val.user.nickname)
        info_box_options.content = infobox_label.replace('{name}', val.user.nickname);
    else
        info_box_options.content = infobox_label.replace('{name}', val.user.first_name + " " + val.user.last_name);
    info_box_options.pixelOffset = info_box_label_offset;
    ib = new InfoBox(info_box_options);
    ib.open(map, val.marker);
    val.infobox_label = ib;
}
