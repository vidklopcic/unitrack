var AUTO_FIT = 1;
var MANUAL_VIEW = 0;
var prev_active;
var map;
var ico1;
var ico2;
var view_mode_button;
var map_zoom_fix = true;
var trafficLayer;

function initMap() {
    // Create a map object and specify the DOM element for display.
    if (map != null) {
        return;
    }
    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src = infobox_url;
    $("head").append(s);
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: 46.0561123, lng: 14.4920594},
        zoom: 12,
        disableDefaultUI: true
    });
    centerDefaultView();
    trafficLayer = new google.maps.TrafficLayer();
    toggleTraffic();
    startMarkers();
}

function setBounds(bounds) {
    map.fitBounds(bounds);
    map_zoom_fix = true;
    google.maps.event.addListener(map, 'bounds_changed', function () {
        if (map_zoom_fix) {
            map.setZoom(map.getZoom() + 1);
            map_zoom_fix = false;
        }
    });
}

function startMap() {
    if (window.location.hash != "#control-panel") {
        initMap()
    }
}

function addMapPosition(name) {
    var bounds = map.getBounds();
    $.post(api_add_map_view, {
        "lat2": bounds.getNorthEast().lat(),
        "lng2": bounds.getNorthEast().lng(),
        "lat1": bounds.getSouthWest().lat(),
        "lng1": bounds.getSouthWest().lng(),
        "name": name
    }, function (data) {
        updatePositions();
    });
}

function deleteMapPosition(id) {
    $.post(api_delete_position_url, {"pk": id}, function (data) {
        updatePositions();
    });
}

function updatePositions() {
    $.get(get_positions_url, function (data) {
        $('#positions-container').html(data);
    });
}

function updateUsers() {
    $.get(list_users_url, function (data) {
        $("#drivers").html(data);
    });
}

function setViewMode(mode) {
    if (mode == 1) {
        ico2.attr('tooltip', 'center manually');
        ico2.html(manual_position_ico);
        ico1.attr('tooltip', 'center automatically');
        ico1.html(auto_position_ico);
        view_mode_button.html(auto_position_ico);
        view_mode = AUTO_FIT;
    } else {
        ico1.attr('tooltip', 'center manually');
        ico1.html(manual_position_ico);
        ico2.attr('tooltip', 'center automatically');
        ico2.html(auto_position_ico);
        view_mode_button.html(manual_position_ico);
        view_mode = MANUAL_VIEW;
    }
    $.post(api_set_map_state, {"view_mode": view_mode});
}

function setDefaultView() {
    var bounds = map.getBounds();
    lat2 = bounds.getNorthEast().lat();
    lng2 = bounds.getNorthEast().lng();
    lat1 = bounds.getSouthWest().lat();
    lng1 = bounds.getSouthWest().lng();
    $.post(api_set_map_state, {
        "lat2": bounds.getNorthEast().lat(),
        "lng2": bounds.getNorthEast().lng(),
        "lat1": bounds.getSouthWest().lat(),
        "lng1": bounds.getSouthWest().lng()
    });
}

function centerDefaultView() {
    if (lat1) {
        setBounds(new google.maps.LatLngBounds(
            new google.maps.LatLng(lat1, lng1),
            new google.maps.LatLng(lat2, lng2))
        )
    }
}

$(document).ready(function () {
    common_init();
    prev_active = $('#control-side-first');
    updateUsers();
    $(".button-collapse").sideNav();
    ico1 = $('#position-ico1');
    ico2 = $('#position-ico2');
    view_mode_button = $('#view-mode-button');
    ico2.click(function () {
        if (view_mode == 0) {
            setViewMode(1);
        } else {
            setViewMode(0);
        }
    });
    $('.prevent-click').click(function (event) {
        event.stopPropagation();
        //Do whatever you want
    });

});

function toggleTraffic() {
    if (trafficLayer.getMap() == null) {
        //traffic layer is disabled.. enable it
        trafficLayer.setMap(map);
    } else {
        //traffic layer is enabled.. disable it
        trafficLayer.setMap(null);
    }
}