from __future__ import unicode_literals

from django.db import models

from UniTrack import settings


class MapState(models.Model):
    lat1 = models.FloatField(null=True, blank=True)
    lng1 = models.FloatField(null=True, blank=True)
    lat2 = models.FloatField(null=True, blank=True)
    lng2 = models.FloatField(null=True, blank=True)
    view_mode = models.IntegerField(default=0)


class ViewLocation(models.Model):
    user = models.ForeignKey(settings.AUTH_USER_MODEL)
    name = models.CharField(max_length=250)
    lat1 = models.FloatField(null=True, blank=True)
    lng1 = models.FloatField(null=True, blank=True)
    lat2 = models.FloatField(null=True, blank=True)
    lng2 = models.FloatField(null=True, blank=True)