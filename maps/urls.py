from django.conf.urls import url

import maps.views

urlpatterns = [
    url(r'^get-positions/$', maps.views.get_positions, name='maps-get-positions'),
]