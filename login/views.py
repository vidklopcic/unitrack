from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect, HttpResponse
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt
from rest_framework import serializers
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import obtain_auth_token, ObtainAuthToken
from rest_framework.response import Response

from login.models import TrackUser


def auth_user(request):
    context = {'redirect_url': request.GET.get('next', '/'),
               'email': request.POST.get('email', ''),
               'password': '',
               'wrong_mail': False,
               'wrong_password': False}
    if request.method == 'POST':
        user = authenticate(email=request.POST['email'], password=request.POST['password'])
        if user and user.is_active and user.is_company():
            login(request, user)
            return redirect(context['redirect_url'])
        if TrackUser.objects.filter(email=request.POST['email']).count():
            context['password'] = request.POST['password']
            context['wrong_password'] = True
        else:
            context['wrong_mail'] = True
    return render(request, 'login.html', context)


def logout_user(request):
    logout(request)
    return redirect(reverse('login'))


class EmailSerializer(serializers.Serializer):
    email = serializers.CharField(label="Email")
    password = serializers.CharField(label="Password", style={'input_type': 'password'})

    def validate(self, attrs):
        email = attrs.get('email')
        password = attrs.get('password')

        if email and password:
            user = authenticate(email=email, password=password)

            if user:
                # From Django 1.10 onwards the `authenticate` call simply
                # returns `None` for is_active=False users.
                # (Assuming the default `ModelBackend` authentication backend.)
                if not user.is_active:
                    msg = _('User account is disabled.')
                    raise serializers.ValidationError(msg, code='authorization')
            else:
                msg = _('Unable to log in with provided credentials.')
                raise serializers.ValidationError(msg, code='authorization')
        else:
            msg = _('Must include "email" and "password".')
            raise serializers.ValidationError(msg, code='authorization')

        attrs['user'] = user
        return attrs


class ObtainToken(ObtainAuthToken):
    serializer_class = EmailSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.POST)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        token, created = Token.objects.get_or_create(user=user)
        return Response({'token': token.key})


@csrf_exempt
def obtain_token(request):
    email = request.POST.get('email')
    password = request.POST.get('password')

    if email and password:
        user = authenticate(email=email, password=password)
        if user and user.is_active:
            return HttpResponse('{"token":"' + str(Token.objects.get(user=user)) + '"}')
