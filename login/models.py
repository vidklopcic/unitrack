from __future__ import unicode_literals

from django.contrib.auth.base_user import BaseUserManager
from django.db import models
from django.contrib.auth.models import AbstractUser, Group
from django.conf import settings
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from rest_framework.authtoken.models import Token
import StringIO
from PIL import Image

from location.models import Position
from maps.models import MapState


class Company(models.Model):
    name = models.CharField(max_length=150, unique=True)

    def __str__(self):
        return self.name


class TrackUserManager(BaseUserManager):
    def create_user(self, email, password, company):
        if not email:
            raise ValueError('Users must have an email address')

        user = TrackUser(
            email=self.normalize_email(email),
            company=company
        )

        user.set_password(password)
        user.map_state = MapState.objects.create()
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, first_name="", last_name=""):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """

        company = Company.objects.filter(name="superuser").first()
        if not company:
            company = Company()
            company.name = "superuser"
            company.save()

        user = self.create_user(
            email,
            password,
            company
        )
        user.first_name = first_name
        user.last_name = last_name
        user.is_admin = True
        user.is_superuser = True
        user.is_staff = True
        user.save(using=self._db)
        return user


class TrackUser(AbstractUser):
    username = None
    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name']
    email = models.CharField(max_length=150, unique=True)
    nickname = models.CharField(max_length=150, default="")
    company = models.ForeignKey(Company)
    objects = TrackUserManager()
    profile_picture = models.FileField(upload_to='documents/%Y/%m/%d', blank=True)
    map_state = models.ForeignKey(MapState)
    base_lat = models.FloatField(null=True, blank=True)
    base_lng = models.FloatField(null=True, blank=True)

    def resize_profile_picture(self):
        image_file = StringIO.StringIO(self.profile_picture.read())
        image = Image.open(image_file)
        w, h = image.size

        image = image.resize((200, int(h*(200.0/w))), Image.ANTIALIAS)

        image_file = StringIO.StringIO()
        image.save(image_file, 'JPEG', quality=90)

        self.profile_picture.file = image_file

    def is_company(self):
        print self.groups.all()
        return self.groups.filter(name='companies').exists()

    class Meta:
        permissions = (
            ("can_add_driver", "Can add a driver"),
        )

    def __str__(self):
        cn = "superuser"
        if self.company:
            cn = self.company.name
        return "%s - %s %s, %s" % (cn, self.first_name, self.last_name, self.email)

    def save(self, *args, **kwargs):
        # delete old file when replacing by updating the file
        try:
            this = TrackUser.objects.get(id=self.id)
            if this.profile_picture != self.profile_picture:
                this.profile_picture.delete(save=False)
        except:
            pass  # when new photo then we do nothing, normal case
        super(TrackUser, self).save(*args, **kwargs)


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    if created:
        Token.objects.create(user=instance)
        print "token-created"

@receiver(pre_delete, sender=TrackUser)
def mymodel_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    instance.profile_picture.delete(False)
