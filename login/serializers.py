from django.contrib.staticfiles.templatetags.staticfiles import static
from rest_framework import serializers

from login.models import TrackUser


class TrackUserSerializer(serializers.ModelSerializer):
    company = serializers.SerializerMethodField()
    profile_picture = serializers.SerializerMethodField()

    def get_company(self, obj):
        return obj.company.name

    def get_profile_picture(self, obj):
        if not obj.profile_picture:
            return static('user-placeholder.jpg')
        return obj.profile_picture.url

    class Meta:
        model = TrackUser
        fields = ('first_name', 'last_name', 'email', 'company', 'profile_picture', 'nickname')
