from django.contrib import admin
from login.models import TrackUser, Company

# Register your models here.

my_models = [TrackUser, Company]
admin.site.register(my_models)