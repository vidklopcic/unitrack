from django.conf.urls import url
import api.views
from rest_framework.authtoken import views

import login.views

urlpatterns = [
    url(r'^set/location/$', api.views.set_location, name='api-set-location'),
    url(r'^path/$', api.views.set_path, name='api-get-last-path'),
    url(r'^location/$', api.views.get_locations, name='api-get-location'),
    url(r'^user/$', api.views.get_user, name='api-get-user'),
    url(r'^login/', login.views.obtain_token, name='get-token'),
    url(r'^map/add-view/', api.views.add_center_location, name='api-add-map-view'),
    url(r'^map/set-state/', api.views.set_map_state, name='api-set-map-state'),
    url(r'^map/get-views/', api.views.get_locations, name='api-get-map-views'),
    url(r'^map/get-state/', api.views.get_map_state, name='api-get-map-state'),
    url(r'^map/delete-position/', api.views.delete_position, name='api-delete-position'),
]