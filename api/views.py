from django.http import HttpResponse
from rest_framework.decorators import api_view
from rest_framework.response import Response

from location.models import Position
from location.serializers import PositionSerializer, PathSerializer
from login.models import TrackUser
from login.serializers import TrackUserSerializer
from maps.models import ViewLocation
from maps.serializers import ViewLocationSerializer, MapStateSerializer


@api_view(['POST'])
def set_location(request):
    position = PositionSerializer(data=request.data)
    position.is_valid(raise_exception=True)
    position.save(user=request.user)
    return Response(position.data)


@api_view(['POST'])
def set_path(request):
    path = PathSerializer(data=request.data)
    path.is_valid(raise_exception=True)
    path.save(user=request.user)
    return Response(path.data)


@api_view(['GET'])
def get_locations(request):
    drivers = TrackUser.objects.filter(company=request.user.company, groups__name="drivers")
    locations = []
    for driver in drivers:
        position = Position.objects.filter(user=driver).last()
        if position:
            location = PositionSerializer()
            if location.get_age(position) < 3600:
                locations.append({'location': PositionSerializer(Position.objects.filter(user=driver).last()).data,
                                  'user': TrackUserSerializer(driver).data})
    return Response(locations)


@api_view(['GET'])
def get_user(request):
    return Response(TrackUserSerializer(request.user).data)


@api_view(['POST'])
def add_center_location(request):
    view_location = ViewLocationSerializer(data=request.data)
    view_location.is_valid(raise_exception=True)
    view_location.save(user=request.user)
    return Response(view_location.data)


@api_view(['GET'])
def get_center_locations(request):
    view_locations = ViewLocation.objects.filter(user=request.user)
    response = []
    for view_location in view_locations:
        response.append(ViewLocationSerializer(view_location))
    return Response(response)


@api_view(['GET'])
def get_map_state(request):
    return Response(MapStateSerializer(request.user.map_state).data)


@api_view(['POST'])
def set_map_state(request):
    map_state = MapStateSerializer(request.user.map_state, data=request.data)
    map_state.is_valid(raise_exception=True)
    map_state.save()
    return Response(map_state.data)


@api_view(['POST'])
def delete_position(request):
    ViewLocation.objects.get(pk=request.POST["pk"]).delete()
    return HttpResponse("ok")
